<?php


namespace Firewox\BigJSON;


use Firewox\BigJSON\Exceptions\ElementClosed;
use Firewox\BigJSON\Exceptions\InvalidScalarType;

class WriterScalar
{

  const TYPE_STRING = 100;
  const TYPE_NUMBER = 101;
  const TYPE_BOOLEAN = 102;
  const TYPE_NULL = 103;

  private $writer;
  private $isClosed;
  private $value;
  private $type;

  public function __construct(Writer $writer)
  {

    $this->writer = $writer;

  }


  public function __destruct()
  {
    // Close when object destroyed
    $this->close();
  }


  public function open(?string $name = null)
  {

    if($this->isClosed) throw new ElementClosed();

    // Set property name if needed
    if(!!$name) $this->writer->writePropertyName($name);

  }


  public function setValue($value, int $type = self::TYPE_STRING): self {

    $this->type = $type;
    $this->value = $value;
    return $this;

  }


  public function close()
  {

    if($this->isClosed) return;

    switch($this->type) {
      case self::TYPE_STRING:
        $this->writer->writeStringValue($this->value);
        break;

      case self::TYPE_NUMBER:
        $this->writer->writeNumberValue($this->value);
        break;

      case self::TYPE_BOOLEAN:
        $this->writer->writeBooleanValue($this->value);
        break;

      case self::TYPE_NULL:
        $this->writer->writeNullValue();
        break;

      default:
        throw new InvalidScalarType();

    }

    // Marl closed
    $this->isClosed = true;

  }

}