<?php


namespace Firewox\BigJSON;


interface Element
{

  public function open(?string $name = null);
  public function close();

}