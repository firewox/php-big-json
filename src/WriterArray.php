<?php


namespace Firewox\BigJSON;


use Firewox\BigJSON\Exceptions\ElementClosed;

class WriterArray implements Element
{

  const EL_OPEN_ARRAY = '[';
  const EL_CLOSE_ARRAY = ']';

  private $writer;
  private $isClosed;
  private $elements = 0;

  /**
   * @var Element
   */
  private $current;


  public function __construct(Writer $writer)
  {

    $this->writer = $writer;

  }


  public function __destruct()
  {
    // Close object
    $this->close();
  }


  public function open(?string $name = null)
  {

    if($this->isClosed) throw new ElementClosed();

    // Set property name if needed
    if(!!$name) $this->writer->writePropertyName($name);

    // Open array value
    $this->writer->write(self::EL_OPEN_ARRAY);

  }


  public function close()
  {

    if($this->isClosed) return;

    // Close current item if any
    if(!!$this->current) $this->current->close();

    // Close array value
    $this->writer->write(self::EL_CLOSE_ARRAY);

    // Marl closed
    $this->isClosed = true;

  }


  private function comma(): void {

    // Set comma if we have set elements before
    if($this->elements > 0) $this->writer->comma();
    $this->elements++;

  }


  public function addObjectItem(): WriterObject {

    $this->current = new WriterObject($this->writer);
    $this->comma();
    $this->current->open();
    return $this->current;

  }


  public function addArrayItem(): WriterArray {

    $this->current = new WriterArray($this->writer);
    $this->comma();
    $this->current->open();
    return $this->current;

  }


  public function addScalarItem(): WriterScalar {

    $this->current = new WriterScalar($this->writer);
    $this->comma();
    $this->current->open();
    return $this->current;

  }

}