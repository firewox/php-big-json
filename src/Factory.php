<?php


namespace Firewox\BigJSON;


use Psr\Http\Message\StreamInterface;

class Factory
{

  /**
   * @var StreamInterface|null
   */
  private $psrStream;

  private $fileHandle;

  /**
   * Factory constructor.
   */
  private function __construct() { }


  public static function createWriterFromFilename(string $filename): Writer {

    $self = new self();
    $self->fileHandle = fopen($filename, 'a');
    return new Writer($self);

  }


  public static function createWriterPsrStream(StreamInterface $stream): Writer {

    $self = new self();
    $self->psrStream = $stream;
    return new Writer($self);

  }


  public function writeOut(string $content): bool {

    if(!!$this->fileHandle) {

      fwrite($this->fileHandle, $content);
      return true;

    } else if(!!$this->psrStream) {

      $this->psrStream->write($content);
      return true;

    }

    return false;

  }


  public function __destruct()
  {
    if(!!$this->fileHandle) fclose($this->fileHandle);
  }

}