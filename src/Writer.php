<?php


namespace Firewox\BigJSON;


use Firewox\BigJSON\Exceptions\RootElementDefined;

class Writer
{

  const CHAR_QUOTE = '"';
  const CHAR_COMMA = ',';
  const CHAR_COLON = ':';
  const BOOL_TRUE = 'true';
  const BOOL_FALSE = 'false';

  private $factory;
  private $root;
  private $isStripNulls;

  public function __construct(Factory $factory)
  {

    $this->factory = $factory;

  }


  public function __destruct()
  {
    $this->close();
  }


  private function isAssociativeArray($data): bool {
    return is_array($data) && !isset($data[0]) && count($data) > 0;
  }


  private function isIndexArray($data): bool {
    return is_array($data) && (isset($data[0]) || count($data) == 0);
  }


  private function isObjectIterable($data): bool {
    return is_object($data) && is_iterable($data);
  }


  private function addArrayAsRoot(): WriterArray {

    // Create root element
    if(!!$this->root) throw new RootElementDefined();
    $this->root = new WriterArray($this);
    $this->root->open();

    return $this->root;

  }


  private function addObjectAsRoot(): WriterObject {

    // Create root element
    if(!!$this->root) throw new RootElementDefined();
    $this->root = new WriterObject($this);
    $this->root->open();

    return $this->root;

  }


  private function processArray($data, ?WriterArray $handler = null)
  {

    // Make sure array that came if correct format
    // And this is root
    if(!isset($data[0]) && !$handler) return $this->processObject($data);


    if(!$handler) {
      $handler = $this->addArrayAsRoot();
    }

    // Iterate through data
    foreach($data as $key => $value) {

      if(is_numeric($key) && is_scalar($value)) {

        $this->processScalar($value, $handler->addScalarItem());

      } else if(is_numeric($key) && is_null($value)) {

        if($this->isStripNulls) continue;
        $this->processScalar($value, $handler->addScalarItem());

      } else if(is_numeric($key) && $this->isIndexArray($value)) {

        $this->processArray($value, $handler->addArrayItem());

      } else if(is_numeric($key) && $this->isObjectIterable($value)) {

        $this->processIterable($value, $handler->addArrayItem());

      } else if(is_numeric($key) && (is_object($value) || !$this->isIndexArray($value))) {

        $this->processObject((object) $value, $handler->addObjectItem());

      } else if(is_string($key)) {

        $this->processObject((object)[$key => $value], $handler->addObjectItem());

      }

    }

    // Close if handler is defined
    !!$handler &&
    $handler->close();

    // Complete
    return true;

  }


  private function processObject($data, ?WriterObject $handler = null)
  {

    if(!$handler) {
      $handler = $this->addObjectAsRoot();
    }

    // Create root level item
    $data = get_object_vars($data);

    // Iterate through data
    foreach($data as $key => $value) {

      if(is_scalar($value)) {

        $this->processScalar($value, $handler->addScalarProperty($key));

      }  else if(is_null($value)) {

        if($this->isStripNulls) continue;
        $this->processScalar($value, $handler->addScalarProperty($key));

      } else if($this->isIndexArray($value)) {

        $this->processArray($value, $handler->addArrayProperty($key));

      } else if($this->isObjectIterable($value)) {

        $this->processIterable($value, $handler->addArrayProperty($key));

      } else if(is_object($value) || $this->isAssociativeArray($value)) {

        $this->processObject((object) $value, $handler->addObjectProperty($key));

      } else {

        $this->processScalar(gettype($value), $handler->addScalarProperty($key));

      }

    }

    // Close if handler is defined
    !!$handler &&
    $handler->close();

    // Complete
    return true;

  }


  private function processIterable($data, ?WriterArray $handler = null)
  {

    if(!$handler) {
      $handler = $this->addArrayAsRoot();
    }

    // Iterate through data
    foreach($data as $value) {

      if(is_scalar($value)) {

        $this->processScalar($value, $handler->addScalarItem());

      } else if(is_null($value)) {

        if($this->isStripNulls) continue;
        $this->processScalar($value, $handler->addScalarItem());

      } else if($this->isIndexArray($value)) {

        $this->processArray($value, $handler->addArrayItem());

      } else if($this->isObjectIterable($value)) {

        $this->processIterable($value, $handler->addArrayItem());

      } else if(is_object($value) || $this->isAssociativeArray($value)) {

        $this->processObject((object) $value, $handler->addObjectItem());

      }

    }

    // Close if handler is defined
    !!$handler &&
    $handler->close();

    // Complete
    return true;

  }


  private function processScalar($data, ?WriterScalar $handler = null)
  {

    // If no handler set, return nothing
    if(!$handler) return $this->write($data);

    $type = is_string($data) ? WriterScalar::TYPE_STRING : null;
    $type = is_numeric($data) ? WriterScalar::TYPE_NUMBER : $type;
    $type = is_bool($data) ? WriterScalar::TYPE_BOOLEAN : $type;
    $type = is_null($data) ? WriterScalar::TYPE_NULL : $type;


    if($type) {
      $handler->setValue($data, $type);
      $handler->close();
    } else if(!$type) {
      return $this->write('null');
    }

    // Complete
    return false;

  }


  public function encode($data, bool $stripNulls = false): bool {

    $this->isStripNulls = $stripNulls;

    // Process objects and arrays only
    if($this->isIndexArray($data)) {

      return $this->processArray($data);

    }else if($this->isObjectIterable($data)) {

      return $this->processIterable($data);

    }else if(is_object($data) || $this->isAssociativeArray($data)) {

      return $this->processObject((object) $data);

    } else if (is_scalar($data)) {

      return $this->processScalar($data);

    }

    return false;

  }


  public function write(string $content): bool {

    return $this->factory->writeOut($content);

  }


  public function close() {

    // Close root
    $this->root &&
    $this->root->close();

  }


  public function comma() {

    $this->write(self::CHAR_COMMA);

  }


  public function writePropertyName(string $name) {

    // OPEN CLOSE QUOTE
    $this->write(self::CHAR_QUOTE);
    $this->write($name);
    $this->write(self::CHAR_QUOTE);

    // COLON TO PREPARE FOR VALUE
    $this->write(self::CHAR_COLON);

  }


  public function writeStringValue(string $value) {

    //OPEN CLOSE QUOTE
    $this->write(json_encode($value));

  }


  public function writeNumberValue($value) {

    $this->write(is_int($value) ? sprintf('%d', $value) : $value);

  }


  public function writeBooleanValue(bool $value) {

    $this->write($value ? self::BOOL_TRUE : self::BOOL_FALSE);

  }


  public function writeNullValue() {

    $this->write('null');

  }


}