<?php

namespace Firewox\BigJSON\Exceptions;

class InvalidScalarType extends \Exception
{

  public function __construct()
  {

    parent::__construct('Scalar type specified is invalid.');

  }

}