<?php

namespace Firewox\BigJSON\Exceptions;

class ElementClosed extends \Exception
{

  public function __construct()
  {

    parent::__construct('Element has been closed.');

  }

}