<?php

namespace Firewox\BigJSON\Exceptions;

class RootElementDefined extends \Exception
{

  public function __construct()
  {

    parent::__construct('Root element is already defined.');

  }

}