<?php


namespace Firewox\BigJSON;


use Firewox\BigJSON\Exceptions\ElementClosed;

class WriterObject implements Element
{

  const EL_OPEN_OBJECT = '{';
  const EL_CLOSE_OBJECT = '}';

  private $writer;
  private $isClosed;
  private $properties = 0;

  /**
   * @var Element
   */
  private $current;

  public function __construct(Writer $writer)
  {

    $this->writer = $writer;

  }


  public function open(?string $name = null)
  {

    if($this->isClosed) throw new ElementClosed();

    // Set property name if needed
    if(!!$name) $this->writer->writePropertyName($name);

    // Open array value
    $this->writer->write(self::EL_OPEN_OBJECT);

  }


  public function close()
  {

    if($this->isClosed) return;

    // Close current item if any
    if(!!$this->current) $this->current->close();

    // Close array value
    $this->writer->write(self::EL_CLOSE_OBJECT);

    // Marl closed
    $this->isClosed = true;

  }


  private function comma(): void {

    // Set comma if we have set elements before
    if($this->properties > 0) $this->writer->comma();
    $this->properties++;

  }


  public function addObjectProperty(string $name): WriterObject {

    $this->current = new WriterObject($this->writer);
    $this->comma();
    $this->current->open($name);
    return $this->current;

  }


  public function addArrayProperty(string $name): WriterArray {

    $this->current = new WriterArray($this->writer);
    $this->comma();
    $this->current->open($name);
    return $this->current;

  }


  public function addScalarProperty(string $name): WriterScalar {

    $this->current = new WriterScalar($this->writer);
    $this->comma();
    $this->current->open($name);
    return $this->current;

  }

}