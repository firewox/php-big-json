<?php

namespace FirewoxTests;

use Firewox\BigJSON\Factory;
use PHPUnit\Framework\TestCase;

class JsonWriterTest extends TestCase
{

  public function testStripNulls() {

    $jsonFile = 'strip_nulls.json';
    if(file_exists($jsonFile)) unlink($jsonFile);

    $writer = Factory::createWriterFromFilename($jsonFile);
    $writer->encode(['test' => 'test', 'test2' => null ], true);

    $this->assertSame('{"test":"test"}', file_get_contents($jsonFile));

  }

  public function testNumberFormat() {

    $jsonFile = 'number_format.json';
    if(file_exists($jsonFile)) unlink($jsonFile);

    $writer = Factory::createWriterFromFilename($jsonFile);
    $writer->encode(['test' => 'test', 'test2' => 744289940276243 ], true);

    $this->assertSame('{"test":"test","test2":744289940276243}', file_get_contents($jsonFile));

  }

  public function testEmptyArray() {

    $jsonFile = 'empty_array.json';
    if(file_exists($jsonFile)) unlink($jsonFile);

    $writer = Factory::createWriterFromFilename($jsonFile);
    $writer->encode(['test' => []], true);

    $this->assertSame('{"test":[]}', file_get_contents($jsonFile));

  }

}
